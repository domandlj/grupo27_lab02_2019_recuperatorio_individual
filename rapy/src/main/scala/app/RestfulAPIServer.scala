package app

import cask._
import models._
import DotProduct.dotProduct



object RestfulAPIServer extends MainRoutes  {
  override def host: String = "0.0.0.0"
  override def port: Int = 4000

  @get("/")
  def root(): Response = {
    JSONResponse("Welcome to Rapy!")
  }
  
  // 1. HTTP methods for Locations.
  @get("/api/locations")
  def locations(): Response = {
    JSONResponse(Location.all.map(location => location.toMap))
  }

  @postJson("/api/locations")
  def locations(name: String, coordX: Int, coordY: Int): Response = {
    
    // Error handling.
    if (Location.exists("name",name)) {
      return JSONResponse("Existing location", 409)
    }

    val location = Location(name, coordX, coordY)
    location.save()
    JSONResponse(location.id)
  }
  

  // 2. HTTP methods for Consumers (user).
  @get("/api/consumers")
  def consumers(): Response = {
    JSONResponse(Consumer.all.map(consumer => consumer.visible))
  }

  @postJson("/api/consumers")
  def consumers(username: String, locationName: String): Response = {
    
    // Error handling.
    if (Consumer.exists("username",username)) {
      return JSONResponse("Existing username", 409)
    }
    if (!Location.exists("name",locationName)) {
      return JSONResponse("Non existing location", 404)
    }
    val locationId = Location.getId("name", locationName)
    val consumer = Consumer(username,locationId,0)
    consumer.save()
    JSONResponse(consumer.id)
  }

  // 3. HTTP methods for Providers (user).
 
  @get("/api/providers")
  def providers(locationName: String): Response = {
    // Error handling.
    if (!Location.exists("name",locationName)) {
      return JSONResponse("Non existing location", 404)
    }
    
    val locationId = Location.getId("name",locationName)
    val providers = Provider.filter(Map("locationId"-> locationId))
    JSONResponse(providers.map(provider => provider.visible))
  } 

  @postJson("/api/providers")
  def providers(username: String, storeName: String,
   locationName: String, maxDeliveryDistance: Int): Response = {
    
    // Error handling.
    if (maxDeliveryDistance < 0) {
      return JSONResponse("Negative max delivery distance", 400)
    }

    if (Provider.exists("username",username) || 
      Provider.exists("storeName", storeName)) {
      return JSONResponse("Existing username or store name", 409)
    }

    if (!Location.exists("name",locationName)) {
      return JSONResponse("Non existing location", 404)
    }
    
    val locationId = Location.getId("name",locationName)
    val provider = Provider(username, locationId , storeName, maxDeliveryDistance,0)
    provider.save()
    JSONResponse(provider.id)
  }

  // 4. HTTP method for deleting an user (consumer or provider).
  @post("/api/users/delete/:username")
  def users(username: String): Response = {
    // Error handling.
    if (!Consumer.exists("username",username) && 
        !Provider.exists("username",username)) {
      return JSONResponse("Non existing user", 409)
    }
    
    // Succes cases.
    if (Consumer.exists("username",username)){
      val consumerId = Consumer.getId("username",username)
      Consumer.delete(consumerId)
      return JSONResponse("Ok", 200)
    }
    if (Provider.exists("username",username)){
      val providerId = Provider.getId("username",username)
      Provider.delete(providerId)
      return JSONResponse("Ok", 200)

    }    
  }

  //5. HTTP method for items.

  @get("/api/items")
  def items(providerUsername: String): Response = {
    
    // Error handling.
    if (!Provider.exists("username",providerUsername)){
      return JSONResponse("Non existing provider", 404)
    }
    val providerId = Provider.getId("username", providerUsername)

    JSONResponse(Item.filter(Map("providerId"-> providerId)))
  }

  @postJson("/api/items")
  def items(name: String, description: String,
   price: Float, providerUsername: String): Response = {
    
    // Error handling.
    if (!Provider.exists("username",providerUsername)){
      return JSONResponse("Non existing provider", 404)
    }       

    // If the provider exists, we fetch his id. 
    val providerId = Provider.getId("username", providerUsername)

    if (!Item.filter(Map("name"->name, "providerId"->providerId)).isEmpty){
      return JSONResponse("Existing item for provider", 409)
    }  

    if (price < 0 ){
      return JSONResponse("Negative price", 400)
    }

    val item = Item(name, price, 
      description, providerId)
    item.save()

    JSONResponse("ok",200)
  }

  @post("/api/items/delete/:id")
  def items(id: Int): Response = {

    // Error handling
    if (!Item.exists("id",id)){
      return JSONResponse("non existing item", 404)
    }
    Item.delete(id)

    JSONResponse("ok",200)
  }

  // Item aux function.
  def getItemPrice(name: String, providerUsername: String): Float = {
    val providerId = Provider.getId("username",providerUsername)
    val result = Item.filterFirst(
      Map("name"-> name,"providerId"-> providerId
      )).price
    return result
  }

  //6. HTTP methods for orders.

  @get("/api/orders/detail/:id")
  def orders(id: Int): Response = {
    if (!Order.exists("consumerId",id)){
      return JSONResponse("Non existing order", 404)
    }
    // Succes cases.
    val consumerOrders = Order.filter(Map("consumerId"->id))
    
    // We serialize the response specified for the API here using data 
    // from Item and Order models.
    JSONResponse(consumerOrders.map(order => (order.items.map(item => Map(
      "name"-> item.name,
      "amount"-> item.amount,
      "price"->  Item.filterFirst(Map("name"-> item.name)).price,
      "description" -> Item.filterFirst(Map("name"-> item.name)).description,
      "id" -> Item.filterFirst(Map("name"-> item.name)).id
      )))))
  }

  @get("/api/orders")
  def orders(username: String): Response = {
    
    // Error handling.
    val consumerId = Consumer.getId("username",username);
    if (!Consumer.exists("id",consumerId)){
      return JSONResponse("Non existing user", 404)
    }

    if (!Order.exists("consumerUsername",username)){
      return JSONResponse("Consumer hasn't orderer anything", 404)
    }
    // Succes cases.
     val orders = Order.filter(Map("consumerUsername"-> username))
     JSONResponse(orders.map(order => order.visible))
  }

  @post("/api/orders/deliver/:id")
  def ordersDeliver(id: Int): Response = {
    // Error handling.
    if (!Order.exists("id",id)){
      return JSONResponse("Non existing order ", 404)
    }
    // Succes cases.
     val order = Order.find(id).get
     order.deliver
     JSONResponse("ok",200)
  }

  @post("/api/orders/delete/:id")
  def ordersDelete(id: Int): Response = {
    // Error handling.
    if (!Order.exists("id",id)){
      return JSONResponse("Non existing order ", 404)
    }
    // Succes cases.
     val order = Order.find(id).get
     order.finish
     JSONResponse("ok",200)
  }


  @postJson("/api/orders")
  def orders(providerUsername: String, consumerUsername: String, 
  items: List[ItemsOrder]): Response = {

    // Error handling.
    if (items.exists(item => item.amount < 0)){
      return JSONResponse("Negative amount", 400)
    }


     if (!Consumer.exists("username",consumerUsername) || 
         !Provider.exists("username",providerUsername)) {
      return JSONResponse("Non existing provider or consumer", 404)
    }

    // Check if the provider has all the items. 
    val providerId = Provider.getId("username", providerUsername)
    val itemsofProviderSet = Item.filter(Map("providerId"-> providerId)).map(
      item => item.name).toSet
    val itemsRequestedSet = items.map(item => item.name).toSet
    
    // The names of the val's tells u what's happening.
    if (!itemsRequestedSet.subsetOf(itemsofProviderSet)) {
      return JSONResponse("Non existing item for provider", 404)
    }

    // Succes cases.

    // Maths for the items.
    val prices: List[Float] = items.map(item => getItemPrice(item.name, providerUsername))
    val amounts: List[Float] = items.map(item => item.amount.toFloat)
    val total = dotProduct(prices,amounts)

    val consumerId = Consumer.getId("username", consumerUsername)
    val consumer = Consumer.find(consumerId).get
    val consumerLocation = Location.find(consumer.locationId).get.name
    val provider = Provider.find(providerId).get
    provider.updateBalance(total)
    consumer.updateBalance(total)


    val order = Order(consumerId, consumerUsername, 
      consumerLocation, providerId,  
      provider.storeName, total , "payed",items)
    
    order.save()

    JSONResponse(order.id)
  }

  override def main(args: Array[String]): Unit = {
    System.err.println("\n " + "=" * 39)
    System.err.println(s"| Server running at http://$host:$port ")

    if (args.length > 0) {
      val databaseDir = args(0)
      Database.loadDatabase(databaseDir)
      System.err.println(s"| Using database directory $databaseDir ")
    } else {
      Database.loadDatabase()  // Use default location
    }
    System.err.println(" " + "=" * 39 + "\n")

    super.main(args)
  }

  initialize()
}
