package app

// Cool zip stolen from Haskell that we will use.
object DotProduct {
  def zipWith[A, B, C](xs: List[A], ys: List[B])(f: (A, B) => C): List[C] = {
  xs.zip(ys).map { tuple =>
    f(tuple._1, tuple._2)
    }
  }

  def dotProduct (xs : List[Float], ys: List[Float]) : Float =
  zipWith(xs,ys)(_*_).foldLeft(0.toFloat)(_+_)
}