package models

object Consumer extends ModelCompanion[Consumer] {
  protected def dbTable: DatabaseTable[Consumer] = Database.consumers

   def apply(username: String, locationId: Int,  balance: Float): Consumer =
     new Consumer(username, locationId, balance)

   private[models] def apply(jsonValue: JValue): Consumer = {
     val value = jsonValue.extract[Consumer]
     value._id = (jsonValue \ "id").extract[Int]
     value.save()
     value
   }
 }

class Consumer(val username: String, val locationId: Int, 
  var balance: Float) extends Model[Consumer] {
  protected def dbTable: DatabaseTable[Consumer] = Consumer.dbTable

  // All this would be saved in a toy json db. 
  override def toMap: Map[String, Any] = super.toMap ++ Map(
    "username" -> username, "locationId" -> locationId, "balance" -> balance)

  // Balance isn't visible when we do a GET.
  def visible: Map[String, Any] = toMap.filterKeys(Set("username", "locationId"))

  override def toString: String = s"Consumer: $username"
  
  

  def updateBalance(price: Float): Unit = {
    this.balance = this.balance - price
    this.save()
  }
}