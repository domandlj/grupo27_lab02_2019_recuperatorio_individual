package models

trait ModelCompanion[M <: Model[M]] {
  protected def dbTable: DatabaseTable[M]

  private[models] def apply(jsonValue: JValue): M

  def all: List[M] = dbTable.instances.values.toList

  def find(id: Int): Option[M] = {
    val instanceList = all.filter(instance => instance.id == id)
    if (!instanceList.isEmpty){
      return Some(instanceList(0))
    } else {
      return None
    }
  } 
 
  def exists(attr: String, value: Any): Boolean = 
    dbTable.instances.values.exists(instance => instance.toMap.get(attr) == Some(value))


  def delete(id: Int): Unit = dbTable.delete(id)

  
  def filter(mapOfAttributes: Map[String, Any]): List[M] =
     all.filter(x => x.toMap.toList.filter(y => 
     mapOfAttributes.toList.contains(y)).toMap == mapOfAttributes)
  
  // Same as filter but get the first model of the list.
  def filterFirst(mapOfAttributes: Map[String, Any]): M =
     all.filter(x => x.toMap.toList.filter(y => 
     mapOfAttributes.toList.contains(y)).toMap == mapOfAttributes)(0)
  
  def getId(attr : String, value: String) : Int = filter(Map(attr-> value))(0).id

  

}

trait Model[M <: Model[M]] { self: M =>
  protected var _id: Int = 0

  def id: Int = _id

  protected def dbTable: DatabaseTable[M]

  def toMap: Map[String, Any] = Map("id" -> _id)

  def save(): Unit = {
    if (_id == 0) { _id = dbTable.getNextId }
    dbTable.save(this)
  }
}
