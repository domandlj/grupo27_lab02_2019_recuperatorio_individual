package models

object Provider extends ModelCompanion[Provider] {
  protected def dbTable: DatabaseTable[Provider] = Database.providers

   def apply(username: String, locationId: Int, storeName: String, 
    maxDeliveryDistance: Int, balance: Float): Provider =
     new Provider(username, locationId, storeName, maxDeliveryDistance, balance)

   private[models] def apply(jsonValue: JValue): Provider = {
     val value = jsonValue.extract[Provider]
     value._id = (jsonValue \ "id").extract[Int]
     value.save()
     value
   }
 }

class Provider(val username: String, val locationId: Int, val storeName: String, 
  val maxDeliveryDistance: Int, private var balance: Float) extends Model[Provider] {
  protected def dbTable: DatabaseTable[Provider] = Provider.dbTable

  // All this would be saved in a toy json db. 
  override def toMap: Map[String, Any] = super.toMap ++ Map(
    "username" -> username, 
    "locationId" -> locationId, 
    "storeName" -> storeName,
    "maxDeliveryDistance" -> maxDeliveryDistance,
    "balance" -> balance
    )
  
  // Balance isn't visible when we do a GET.
  def visible: Map[String, Any] = toMap.filterKeys(Set(
    "username", "locationId","storeName",
    "maxDeliveryDistance","maxDeliveryDistance" ))

  override def toString: String = s"Provider: $username"

  def updateBalance(price: Float): Unit = {
    this.balance = this.balance + price
    this.save()
  }
}