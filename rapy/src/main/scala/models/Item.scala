package models

object Item extends ModelCompanion[Item] {
  protected def dbTable: DatabaseTable[Item] = Database.items

   def apply(name: String, price: Float, 
      description: String, providerId: Int): Item =
     new Item(name, price, description, providerId)

   private[models] def apply(jsonValue: JValue): Item = {
     val value = jsonValue.extract[Item]
     value._id = (jsonValue \ "id").extract[Int]
     value.save()
     value
   }
 }

class Item(val name: String, val price: Float, 
  val description: String, val providerId: Int) extends Model[Item] {
  protected def dbTable: DatabaseTable[Item] = Item.dbTable

  override def toMap: Map[String, Any] = super.toMap ++ Map(
    "name"-> name, 
    "price"-> price, 
    "description"-> description, 
    "providerId"-> providerId
    )

  override def toString: String = s"Item: $name"

  
}