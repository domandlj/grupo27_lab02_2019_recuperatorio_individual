package models
import upickle.default.{ReadWriter => RW, macroRW}

case class ItemsOrder(name: String, amount: Int)
object ItemsOrder {
  implicit val rw: RW[ItemsOrder] = macroRW
}

object Order extends ModelCompanion[Order] {
  protected def dbTable: DatabaseTable[Order] = Database.orders

   def apply(consumerId: Int, consumerUsername: String, 
      consumerLocation: String, providerId: Int,  
      providerStoreName: String, orderTotal: Float, 
      status: String, items : List[ItemsOrder]): Order =
     
     new Order(consumerId, consumerUsername, consumerLocation, 
      providerId, providerStoreName, orderTotal, status, items)

   private[models] def apply(jsonValue: JValue): Order = {
     val value = jsonValue.extract[Order]
     value._id = (jsonValue \ "id").extract[Int]
     value.save()
     value
   }
 }

class Order( val consumerId: Int, val consumerUsername: String, 
   val consumerLocation: String, val providerId: Int,  
   val providerStoreName: String, val orderTotal: Float, 
   var status: String, val items : List[ItemsOrder]) extends Model[Order] {
  
  protected def dbTable: DatabaseTable[Order] = Order.dbTable

  override def toMap: Map[String, Any] = super.toMap ++ Map(
    "consumerId"-> consumerId, 
    "consumerUsername"-> consumerUsername, 
    "consumerLocation"-> consumerLocation, 
    "providerId"-> providerId,
    "providerStoreName"-> providerStoreName, 
    "orderTotal"-> orderTotal, 
    "status"-> status,
    "items" -> items
    )
  
  // Items isn't visible when we do a GET.
  def visible: Map[String, Any] = toMap.filterKeys(Set(
    "consumerId", 
    "consumerUsername", 
    "consumerLocation", 
    "providerId",
    "providerStoreName", 
    "orderTotal", 
    "status"))
  
  def deliver(): Unit = {
    this.status = "delivered"
    this.save()
  }
  
  def finish(): Unit = {
    this.status = "finished"
    this.save()
  }

  override def toString: String = s"Order"
  
}