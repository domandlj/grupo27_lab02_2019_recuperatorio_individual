# Rapy

![](rapyLogo.jpg)

Implementación de una **API Rest** de un web-service similar a *Rappi* escrita por Juan Domandl.


# **Prácticas de POO**

La _API_ fue implementada siguiendo el patrón de diseño _MVC_. El estilo de _POO_ que permite **Scala** fue útil para implementar los models (la sigla M del patrón). Estos se encuentran en el directorio models, los cuales son los siguientes

  

*  Model.
*  Location.
*  Consumer.
*  Provider.
*  Item.
*  Order.

  

Se hace uso de _herencia simple_ porque todas las clases que representan modelos heredan de `Model`, éste último contiene un _trait_ `ModelCompanion` en el que se encuentran los métodos incluidos en el esqueleto del código `all`, `find`, `exists`, `delete`, `filter` pero además se agregan dos nuevas funciones `filterFirst` y `getId`. `filterFirst` es exactamente igual a `filter` pero devuelve un modelo.  Todas estas funciones son de gran utilidad para reutilizar código porque son polimórficas, funcionan con todos los objetos `Location`, `Consumer`, etc. que heredan de `Model`.

  

Conceptualmente existen usuarios (`Location` y `Consumer`), pero no se ha creado una clase User de la cual hereden éstas dos últimas para no incurrir en _herencia multiple_ y evitar complejidad innecesaria.

  

Se ha mantenido modularizado el código siguiendo el principio de _single-responsibility_ para cada clase, i.e., que solo tengan un propósito . Además, cada clase cuenta con su archivo distinto. Todas las clases que heredan de `Model` cuentan con una reescritura del método `toMap` para serializar cada objeto-instancia de esa clase en formato `JSON` en una base de datos “de juguete”. Algunas clases como `Provider`, `Consumer` y `Order` cuentan con un método visible específico para pasar un diccionario (Map en la nomenclatura de **Scala**) con la restricción de solo algunos de los atributos que se piden en la especificación de la API y no retornar todos los que tiene el objeto como sucede con `toMap`.

## **Practicas de programación funcional.**

**Scala* se popularizó porque contiene aspectos del paradigma de programación funcional (tipado fuerte, funciones de alto orden, expresiones lambda (anónimas), valores inmutables, etc.). En este proyecto se hizo uso de este paradigma especialmente en los controladores del patrón _MVC_, esto se ve reflejado en el archivo `RestfulAPIServer.scala` en donde se implementa la C del patrón de diseño.

  

En ese archivo se hacen peticiones a todos modelos para controlar las requests y responses HTTP de los _endpoints_ especificados por la API. De esto se encarga el framework **Cask** mediante decoradores @ encima de cada función.

  

En el cuerpo de cada función que implementa un _endpoint_ de la API se tiene un caso de éxito en donde se devuelve un response status como 200 y casos de error 400 si no se satisface alguna de las condiciones. Esto es común a todos los métodos de este archivo.

  

Las practicas programación funcional se usan principalmente acá para manipular la información de models, todo esto a través de un amplio uso de las funciones `map`, `filter` y `zip`. De hecho, en todo el código no se han escrito loops porque se prefirió usar este tipo de funciones de alto orden (tomando como argumento funciones anónimas para filtrar los datos). En particular se implementó una función `zipWith`, como idea robada de Haskell, para escribir una función `dotProduct` que hace el producto escalar entre dos listas sin utilizar un ciclo for o prácticas imperativas. Esta función era necesaria para cálculos sobre los precios y cantidades en `Order`.

  

A lo largo de todo el proyecto la regla ha sido usar tipos de datos inmutables y declarar constantes con `val` y no variables con `var`. La excepción ha sido cuando era estrictamente necesario en algunos atributos de los modelos.

Se ha declarado todos los tipos en las signaturas de funciones.

  

En resumén, se puede decir que para los modelos hemos usado el paradigma orientado a objetos y para los controladores el paradigma de programación funcional.

## **Estilo del código fuente.**

Se ha tratado de usar una cantidad razonable de columnas en el código. Los comentarios son pocos y breves, pero esto no es accidental. Se prefirió dar nombres con sentido a todas las funciones, variables e inclusive variables anónimas para que el comportamiento del código sea inteligible al leer el código. Se ha escrito, por ejemplo, la función `filterFirst` para evitar tener que acceder al primer elemento de las listas explícitamente porque es muy feo a la vista y confuso.

Se ha refactorizado varias veces las expresiones muy largas dividiéndolas en métodos y valores intermedios para que el código sea más legible. Es decir escribir código más largo, pero entendible.


Los métodos de `RestfulAPIServer.scala` se ha preferido mantenerlos lo más acotados posibles, escondiendo la lógica de negocio en los modelos o en archivos apartes.
## **Testing.**

  

Se ha hecho client testing de la especificación de la API usando **Postman**, se escribieron varias requests de todos los métodos HTTP GET y POST, de los casos de éxito y casos de fallas. **Postman** permite escribir scripts con casos de test usando **JavaScript**. Se escribieron tests que chequean todos los response. Los casos de tests consisten en verificar que las response sean 200 o 400s en casos de error; pero además chequear que los modelos de `Order` cambian de estado.


Para probar los tests se debe abrir en Postman el archivo `Rapy.postman_collection.json` que está en la raíz del repositorio. Previamente también se deben borrar los JSON de la base de datos, para esto se puede usar el script de Python: 
`$ python cleanDb.py`.